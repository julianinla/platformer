﻿using UnityEngine;
using System.Collections;

public class Enemy : MovingObject
{

    public bool isEnemyStrong;
    public bool isEnemyAlt;
    public int attackDamage;
    //public AudioClip enemySound1;
    //public AudioClip enemySound2;
    public static int time = 5;
    public float speed = 3f;
    public int dir;
    public float Speed = 0f;

    private float movex = -0f;
    private float movey = 0f;
    private bool skipCurrentMove;
    private Transform player;
    private Transform enemy;
    private Animator animator;
    //private BoxCollider2D boxCollider;
    //private Rigidbody2D rigidBody;
    //private LayerMask collisionLayer;

    // Use this for initialization
    protected override void Start()
    {
        //rigidBody = GetComponent<Rigidbody2D>();
        //collisionLayer = LayerMask.GetMask("Collision Layer");
        player = GameObject.FindGameObjectWithTag("Player").transform;
        enemy = this.transform;
        animator = GetComponent<Animator>();
        base.Start();
    }

    public void MoveEnemy()
    {
        int xAxis = 0;
        int yAxis = 0;

        float xAxisDistance = Mathf.Abs(player.position.x - transform.position.x);
        //float yAxisDistance = Mathf.Abs(player.position.y - transform.position.y);

        

        if (xAxisDistance > 0)
        {
            Debug.Log("moveenemy");
            if (player.position.x > transform.position.x)
            {
                xAxis = 1;
            }
            else
            {
                xAxis = -1;
            }
        }
    
        Move<Player>(xAxis, yAxis);
        //skipCurrentMove = true;
    }
    
    protected override void HandleCollision<T>(T component)
    {
        Debug.Log("collision");
        Player player = component as Player;
        player.TakeDamage(attackDamage);
        animator.SetTrigger("enemyAttack");
        //SoundController.Instance.PlaySingle(enemySound1, enemySound2);
    }/*

    public void Movement()
    {
        this.MoveEnemy();
    } 
    */

    // Update is called once per frame
    void Update()
    {
        //MoveEnemy();
        transform.Translate(-Vector2.right * speed * Time.deltaTime);
        
        //RaycastHit2D hit;
        //bool canMove = CanObjectMove(0, out hit);
        
        ///Player hitComponent = hit.transform.GetComponent<Player>();

        //if (hit.transform.GetComponent<Player>() != null)
        //{
        //    HandleCollision(hit.transform.GetComponent<Player>());
        //}
        //Move<Player>(xAxis, yAxis);
        //movex = Input.GetAxis("Horizontal");
        //movey = Input.GetAxis("Vertical");
        //GetComponent<Rigidbody2D>().velocity = new Vector2(movex * Speed, movey * Speed);
        /*Vector3 moveDirection = player.transform.position ;
        if (moveDirection != Vector3.zero)
        {
            float angle = Mathf.Atan2(moveDirection.y, moveDirection.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        }
        */

        //Invoke("Movement", time);
        //rotate to look at the player
        //transform.LookAt(player);
        //transform.Rotate(new Vector3(0, -90, 0), Space.Self);
        //correcting the original rotation
        /*if(enemy.transform.position.x - player.transform.position.x > 0)
        {
            dir = -1;
        }
        else
        {
            dir = 1;
        }

        //move towards the player
        if (Vector3.Distance(transform.position, player.position) > 1f)
        {//move if distance from target is greater than 1
            //this.MoveEnemy();
          transform.Translate(new Vector3(dir * speed * Time.deltaTime, 0, 0));
        } */
    }
}