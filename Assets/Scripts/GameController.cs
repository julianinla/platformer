﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{

    public static GameController Instance;
    public bool isPlayerTurn;
    public bool areEnemiesMoving;
    public bool levelChange;
    public int playerCurrentHealth = 50;
    public AudioClip gameOverSound;

    public int playerHealth;
    public int money;

    public string sceneBeingUsed;

    private List<Enemy> enemies;
    private GameObject levelImage;
    private Text levelText;
    private GameObject startImage;
    private Text healthText;
    private Text moneyText;
    private bool settingUpGame;
    private GameObject selectScreen;
    private Text startText;
    private Button firstChoice;
    private Button secondChoice;
    private Text choice1;
    private Text choice2;
    private int secondsUntilLevelStart = 2;
    private int currentLevel = 1;
    private bool startScreenOn;

    void Awake()
    {
        if (Instance != null && Instance != this)
        {
            DestroyImmediate(gameObject);
            return;
        }

        Instance = this;
        DontDestroyOnLoad(gameObject);
        enemies = new List<Enemy>();
    }

    void Start()
    {
        InitializeGame();
    }

    private void InitializeGame()
    {
        //startImage = GameObject.Find("Start");
        //startText = GameObject.Find("StartText").GetComponent<Text>();
        //startImage.SetActive(true);
        startScreenOn = true;
    }


    private void DisableStartScreen()
    {
        startImage = GameObject.Find("Start");
        startImage.SetActive(false);
    }

    public void startGame()
    {
    }

    private void OnLevelWasLoaded(int levelLoaded)
    {
        currentLevel++;
        InitializeGame();
    }

    public void ChangeLevel()
    {
        if (Application.loadedLevelName == "Level2")
        {
            Application.LoadLevel("Level1");
            Debug.Log(2);
        }
        else
        {
            Application.LoadLevel("Level2");
            //Debug.Log(Scene.name);
        }
    }
    /*levelImage = GameObject.Find("Level Image");
            levelText = GameObject.Find("Level Text").GetComponent<Text>();
            levelText.text = "Day " + currentLevel;
            levelImage.SetActive(true);*/

    void Update()
    {
        sceneBeingUsed = UnityEditor.EditorApplication.currentScene;

        //if (startScreenOn == true)
        //{
        //  Debug.Log("here");
        //startImage.SetActive(false);
        //Invoke("DisableStartScreen", secondsUntilLevelStart);
        //startScreenOn = false;
        //}

        if (isPlayerTurn || areEnemiesMoving)
        {
            return;
        }
        //else if (isPlayerTurn) {
        //	isPlayerTurn = true;
        //}

        StartCoroutine(MoveEnemies());
    }

    private IEnumerator MoveEnemies()
    {
        foreach (Enemy enemy in enemies)
        {
            enemy.MoveEnemy();
            yield return new WaitForSeconds(enemy.moveTime);
        }
    }

    public void GameOver()
    {
        isPlayerTurn = false;

        //SoundController.Instance.music.Stop();
        //SoundController.Instance.PlaySingle(gameOverSound);

    }
}