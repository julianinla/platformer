﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Player : MovingObject {

    public Text bankText; 
    public int money = 0;
    public int playerHealth = 50;
    public bool playerHurt;
    public bool playerAttack;
    public Text healthText;
    public Text moneyText;
    public int enemyAttackDamage;

    public float Speed = 0f;
    private float movex = 0f;
    private float speed = 4f;

    private Animator animator;
    private Transform enemy;
    private Enemy component;

    // Use this for initialization
    protected override void Start()
    {
        base.Start();
        animator = GetComponent<Animator>();
        healthText = GameObject.Find("HealthText").GetComponent<Text>();
        moneyText = GameObject.Find("MoneyText").GetComponent<Text>();
        healthText.text = "Health: " + playerHealth;
        moneyText.text = "Money: " + money;
    }

    private void OnDisable()
    {
        //GameController.Instance.playerCurrentHealth = playerHealth;
    }

    public void TakeDamage(int damageReceived)
    {
        playerHealth -= damageReceived;
        healthText.text = "Health: " + playerHealth;
        animator.SetTrigger("playerHurt");
    }

    protected override void HandleCollision<T>(T component)
    {
    }

    private void OnTriggerEnter2D(Collider2D objectPlayerCollidedWith)
    {
        if (objectPlayerCollidedWith.tag == "Coin")
        {
            money+=5;
            moneyText.text = "Money: " + money;
            objectPlayerCollidedWith.gameObject.SetActive(false);
        }
    }

    void Update()
    {
        //int xAxis = 0;
        //int yAxis = 0;
        //xAxis = (int)Input.GetAxisRaw("Horizontal");

        //if (xAxis != 0)
        //{
           //Move<Wall>(xAxis, yAxis);
        //}
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.Translate(-Vector2.right * speed * Time.deltaTime);
        }
        else if (Input.GetKey(KeyCode.RightArrow))
        {
           transform.Translate(Vector2.right * speed * Time.deltaTime);
        }
        //if (Input.GetKey(KeyCode.UpArrow))
        //{
         //   transform.Translate(2 * Vector2.up * speed * Time.deltaTime);
        //}
        //else if(Input.GetKey(KeyCode.DownArrow))
        //{
          //  transform.Translate(-Vector2.up * speed * Time.deltaTime); 
        //}
    }
}

